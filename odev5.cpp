#include <iostream>
#include <conio.h>
#include <locale.h>
#define INFINITY 9999
#define MAX 10
using namespace std;

 
void enkisayolalgoritmasi(int G[MAX][MAX],int n,int ilkdugum)
{
 
    int maliyettablosu[MAX][MAX],uzakliktablosu[MAX],dogrulama[MAX];
    int bakilan[MAX],sayac,minmaliyettablosu,sonrakidugum,i,j;
    
	// maliyettablosu dizisinin i�eri�i dolduruluyor.
    for(i=0;i<n;i++)
        for(j=0;j<n;j++)
            if(G[i][j]==0)
                maliyettablosu[i][j]=INFINITY;
            else
                maliyettablosu[i][j]=G[i][j];
    
    // d���mler ile ilgili dizilere de�erler atan�yor.
    for(i=0;i<n;i++)
    {
        uzakliktablosu[i]=maliyettablosu[ilkdugum][i];
        dogrulama[i]=ilkdugum;
        bakilan[i]=0;
    }
    
    uzakliktablosu[ilkdugum]=0;
    bakilan[ilkdugum]=1;
    sayac=1;
    
    while(sayac<n-1)
    {
        minmaliyettablosu=INFINITY;
        // Bir sonraki d���mle olan mesafeler hesaplan�yor.
        for(i=0;i<n;i++)
            if(uzakliktablosu[i]<minmaliyettablosu&&!bakilan[i])
            {
                minmaliyettablosu=uzakliktablosu[i];
                sonrakidugum=i;
            }
            
            // Ba�lant�l� d���mler aras�ndaki en k�sa yol aran�yor.            
            bakilan[sonrakidugum]=1;
            for(i=0;i<n;i++)
                if(!bakilan[i])
                    if(minmaliyettablosu+maliyettablosu[sonrakidugum][i]<uzakliktablosu[i])
                    {
                        uzakliktablosu[i]=minmaliyettablosu+maliyettablosu[sonrakidugum][i];
                        dogrulama[i]=sonrakidugum;
                    }
        sayac++;
    }
 
    // �stenilen d���me a�daki en yak�n yol algoritmas� ekrana yazd�r�l�yor.
    for(i=0;i<n;i++)
        if(i!=ilkdugum)
        {
        	cout<<"\n\t-----------------------------------------------------------\n";
            cout<<"\n\t"<<(char)(i+65)<<" D���m�ne Olan Uzakl�k : "<<uzakliktablosu[i];
            cout<<"\n\tGidi� Yolu = "<<(char)(i+65);
            
            j=i;
            do
            {
                j=dogrulama[j];
                cout<<"\t<-- "<<(char)(j+65);
            }while(j!=ilkdugum);
    }
}
 
int main()
{
	setlocale(LC_ALL, "Turkish");
    int G[MAX][MAX],i,j,dugumsayisi;
    char dugumadi,c;
	char dugum[dugumsayisi];
    cout<<"\n\t----- EN KISA YOL ALGOR�TMASI -----"<<endl;
    cout<< "\n\tA�daki D���m Say�s�n� Giriniz : "; cin>>dugumsayisi;
    cout<<"\n\tD���mler Aras� Maliyetleri Giriniz : \n";
    cout<<"\n\t�rnek : 1 2 0 4 \n\t\t7 0 8 1\n "<<endl;
    for(i=0;i<dugumsayisi;i++){
    	cout<<"\t";
        for(j=0;j<dugumsayisi;j++){
            	cin>>G[i][j];        	
        }
    }
    cout<<"\t-----------------------------------------------------------\n";
    cout<<"\tD���MLER : ";
    for (i=0; i<dugumsayisi; i++){
		dugum[i]=(char)i+65;
    	cout<<" ( "<<dugum[i]<<" ) ";
    }
	cout<<"\n\t-----------------------------------------------------------\n";
    cout<<"\n\tHesaplanacak D���m ( �rnek A ) : ";    cin>>dugumadi;
    enkisayolalgoritmasi(G,dugumsayisi,dugumadi-65);
    cout<<"\n\n";
    return 0;
}
 

