#include <iostream>
#include <conio.h>
#include<locale.h> 
#include <iomanip>
using namespace std;

int main(){
	setlocale(LC_ALL, "Turkish"); 
	double veriboyutu, transferboyutu, transferzamani;
	double verim1, verim2, verim3, verim4;
	cout.precision(10);
	char secim;
	do{
		system("cls");
		cout<<"\t  :::... NETWORK VER�M (THROUGHPUT) HESABI ...:::"<<endl;
		cout<<"\t -------------------------------------------------"<<endl;
		cout<<"\t\t1.  50 RTT  5 Mbps Network Hatt�."<<endl;
		cout<<"\t\t1. 100 RTT 10 Mbps Network Hatt�."<<endl;
		cout<<"\t\t1. 200 RTT 20 Mbps Network Hatt�."<<endl;
		cout<<"\t\t1. 400 RTT 40 Mbps Network Hatt�."<<endl;
		cout<<"\t -------------------------------------------------"<<endl;
		cout<<"\t\tTransfer Edilecek Veri Boyutu (MB) : ";	
		cin>>veriboyutu;
		// 50 RT 5 Mbps ��in Veri hesab� ba�lang��
			transferboyutu=veriboyutu*8;
			transferzamani=0.05+(transferboyutu/5);
			verim1=transferboyutu/transferzamani;
		// biti�
		// 100 RT 10 Mbps ��in Veri hesab� ba�lang��
			transferboyutu=veriboyutu*8;
			transferzamani=0.1+(transferboyutu/10);
			verim2=transferboyutu/transferzamani;		
		// biti�
		// 200 RT 20 Mbps ��in Veri hesab� ba�lang��
			transferboyutu=veriboyutu*8;
			transferzamani=0.2+(transferboyutu/20);
			verim3=transferboyutu/transferzamani;		
		// biti�
		// 400 RT 40 Mbps ��in Veri hesab� ba�lang��
			transferboyutu=veriboyutu*8;
			transferzamani=0.4+(transferboyutu/40);
			verim4=transferboyutu/transferzamani;
		// biti�
		cout<<"\t -------------------------------------------------"<< endl;
		cout<<"\t  1.  50 RTT  5 Mbps ��in Throughput De�eri : "<< setprecision(3) << verim1<< endl;
		cout<<"\t  2. 100 RTT 10 Mbps ��in Throughput De�eri : "<< setprecision(3) << verim2<< endl;	
		cout<<"\t  3. 200 RTT 20 Mbps ��in Throughput De�eri : "<< setprecision(3) << verim3<< endl;
		cout<<"\t  4. 400 RTT 40 Mbps ��in Throughput De�eri : "<< setprecision(3) << verim4<< endl;
		cout<<"\t -------------------------------------------------"<<endl;
		cout<<"\n\t Tekrar Hesap Yapmak �ster Misiniz? [ E / H ] : ";
		cin>>secim;
	}while(secim=='e' || secim=='E');
	return 0;
}
