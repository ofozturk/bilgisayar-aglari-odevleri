#include <stdio.h>
#include <locale.h> 

int main(){
	setlocale(LC_ALL, "Turkish"); 
	int penceretoplamboyutu=10000, pencerebaslangic=0, pencerebitis=0, i=0;
	int pencereboyutu, penceresayisi, pencereuzunluk;
	int penceresonuc, hatalipencereno, ps;
	// girilen pencere boyutu 0'dan k���kse tekrar girilmesi istenecek.
	do{
		printf("\nKullan�lacak Pencere Boyutunu Giriniz : ");
		scanf("%d",&pencereboyutu);
		if (pencereboyutu==0 || pencereboyutu<0){
			printf("\n\n------------------------------------------------------------\n");
			printf("\nUYARI : Kullan�lacak Pencere Boyutu 0'dan B�y�k Olmal�d�r...\n");
			printf("\n------------------------------------------------------------\n\n");
			penceresonuc=0;						 
		}else{
			penceresonuc=1;
		}
	} while (penceresonuc<=0);
	ps=penceretoplamboyutu/pencereboyutu;
	int veriler [ps][3];						// girilen pencere boyutuna uygun sekilde dizi olu�turuluyor. ba�lang��
	pencereuzunluk=pencereboyutu;
	printf("\n------------------------------------------------------------\n\n");
	do{
		i=i+1;
		penceresayisi=penceresayisi+1;
		if (penceretoplamboyutu<pencereuzunluk){
			pencerebitis=10000;
		}
		else{
			pencerebitis=pencereuzunluk;
		}
		// pencere say�s� ba�lang�� ve biti� de�erleri ile birlikte veriler dizisine atan�yor. ba�lang��
		veriler[i][1]=pencerebaslangic;
		veriler[i][0]=penceresayisi;
		veriler[i][2]=pencerebitis;
		// biti�
		printf("\n\t%d",penceresayisi+1);
		printf(". Pencere �letiliyor...");
		printf("\n\tPencerenin Ba�lang�� ve Biti� De�erleri : %d ... %d ",pencerebaslangic,pencerebitis,"\n");
		printf("\n\t----------------------------------------------------------------\n");	
		pencerebaslangic=pencerebitis+1;
		pencereuzunluk=pencerebaslangic+pencereboyutu;
	} while(pencerebitis<penceretoplamboyutu);
	printf("\n\n\t�letimde Kullan�lan Pencere Boyutu : %d ", pencereboyutu);
	printf("\n");
	printf("\n\tKullan�lan Toplam Pencere Say�s�.... : %d ", penceresayisi);
	printf("\n\n");
	printf("\n------------------------------------------------------------\n\n");
	// hatal� al�nan pencere varsa al�c� taraf�ndan numaras� talep ediliyor. ba�lang��
		printf("\n\tVarsa Hatal� Pencere Numaras� ( 0 -> Hata Yok. ��k�� Yap. ) : ");
		scanf("%d",&hatalipencereno);
		
		while(hatalipencereno<0 || hatalipencereno>penceresayisi){
				printf("\n\n\t-----------------------------------------------------------------------------------\n");
				printf("\n\tUYARI : Girilen Pencere Boyutu 0'dan K���k ve Pencere Say�s�ndan B�y�k Olmamal�d�r...\n");
				printf("\n\t-------------------------------------------------------------------------------------\n\n");	
				printf("\n\tVarsa Hatal� Pencere Numaras�n� Tekrar Giriniz ( 0 -> Hata Yok. ) : ");
				scanf("%d",&hatalipencereno);
		}
	// biti�
	if(hatalipencereno!=0){
	//	printf("\n\tHatal� G�nderilen Pencere Numaras� : %d \n", veriler[hatalipencereno][0]);
		printf("\n\tPencerenin Ba�lang�� De�eri....... : %d \n", veriler[hatalipencereno][1]);
		printf("\n\tPencerenin Biti� De�eri........... : %d \n", veriler[hatalipencereno][2]);	
	}
	return 0;
}
